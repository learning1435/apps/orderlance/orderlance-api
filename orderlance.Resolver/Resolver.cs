using Microsoft.Extensions.DependencyInjection;
using orderlance.Abstract.Interfaces;
using orderlance.Dal;
using orderlance.Dal.Repositories;
using orderlance.Services;
using Microsoft.Extensions.Configuration;
using orderlance.Settings;

namespace orderlance.Resolver
{
    public class Resolver
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<Mongo>();
            services.AddScoped<IAuth, Auth>();
            
            services
                .AddScoped<CategoriesRepository>()
                .AddScoped<MealsRepository>()
                .AddScoped<OrdersRepository>()
                .AddScoped<StatisticsRepository>()
                .AddScoped<UsersRepository>();

            services
                .AddScoped<IUsersService, UsersService>()
                .AddScoped<ICategoriesService, CategoriesService>()
                .AddScoped<IMealsService, MealsService>()
                .AddScoped<IOrdersService, OrdersService>()
                .AddScoped<IStatisticsService, StatisticsService>()
                .AddScoped<IAuthService, AuthService>();
        }

        public static void LoadConfiguration(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DatabaseSettings>(configuration.GetSection("Database"));
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
        }
    }
}