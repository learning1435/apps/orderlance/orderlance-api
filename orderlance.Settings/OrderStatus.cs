namespace orderlance.Settings
{
    public class OrderStatus
    {
        public const string UNPAID = "unpaid";
        public const string PAID = "paid";
        public const string IN_DELIVERY = "in_delivery";
        public const string DELIVERED = "delivered";
    }
}