namespace orderlance.Settings
{
    public class Roles
    {
        public const string QUEST = "guest";
        public const string USER = "user";
        public const string ADMIN = "admin";
    }
}