namespace orderlance.Settings
{
    public class DatabaseSettings
    {
        public DatabaseSettings()
        {
        }
        
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}