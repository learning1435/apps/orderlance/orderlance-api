using orderlance.Abstract.Interfaces;
using orderlance.Dto;

namespace orderlance.Services
{
    public class Auth : IAuth
    {
        private UserDto _user;
        
        public void SetUser(UserDto user)
        {
            this._user = user;
        }

        public UserDto User()
        {
            return this._user;
        }
    }
}