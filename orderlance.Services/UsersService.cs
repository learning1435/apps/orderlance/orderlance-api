using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using orderlance.Abstract.Interfaces;
using orderlance.Dal.Entities;
using orderlance.Dal.Repositories;
using orderlance.Dto;
using orderlance.Dto.Request;
using orderlance.Dto.Request.User;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;
using orderlance.Settings;

namespace orderlance.Services
{
    public class UsersService : IUsersService
    {
        private readonly UsersRepository _usersRepository;
        private readonly IAuth _auth;

        public UsersService(UsersRepository usersRepository, IAuth auth)
        {
            this._usersRepository = usersRepository;
            this._auth = auth;
        }
        
        public void RegisterUser(RegisterRequestDto requestDto)
        {
            if (requestDto.Password != requestDto.ConfirmPassword)
                throw new Exception("Passwords are not match!");
            
            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(Encoding.UTF8.GetBytes(requestDto.Password));

            var user = new User()
            {
                Email = requestDto.Email,
                Password = Encoding.UTF8.GetString(md5data),
                Username = requestDto.Username,
                Role = Roles.USER,
                Avatar = "avatar-0"
            };

            this._usersRepository.StoreItem(user);
        }

        public CollectionDto<UserDto> GetUsers(PaginationParamsDto paramsDto)
        {
            var response = new CollectionDto<UserDto>();
            IEnumerable<User> users;
            
            if (paramsDto.Page == null || paramsDto.ItemsPerPage == null)
            {
                users = this._usersRepository.GetAllItems();
                response.TotalItems = users.Count();
            }
            else
            {
                users = this._usersRepository.GetItems(paramsDto.Page.Value, paramsDto.ItemsPerPage.Value);
                response.TotalItems = this._usersRepository.GetItemsCount();
            }

            response.Page = paramsDto.Page ?? 0;
            response.Members = users.Select(this.MapUser);
            
            return response;
        }

        public UserDto GetUserById(string id)
        {
            var user = this._usersRepository.GetUserById(id);

            return new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                Role = user.Role,
                Username = user.Username,
                Avatar = user.Avatar,
            };
        }

        public void UpdateProfile(ChangeProfileRequestDto requestDto)
        {
            var updateUser = new User
            {
                Id = this._auth.User().Id,
                Avatar = requestDto.Avatar,
                Username = requestDto.Username,
            };
            this._usersRepository.UpdateUser(updateUser);
        }

        public void ChangePassword(ChangePasswordRequestDto requestDto)
        {
            throw new NotImplementedException();
        }
        
        private UserDto MapUser(User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                Avatar = user.Avatar,
                Role = user.Role,
                Username = user.Username,
            };
        }
    }
}