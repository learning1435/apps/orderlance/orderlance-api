using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using orderlance.Dto;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using orderlance.Abstract.Interfaces;
using orderlance.Dal.Repositories;
using orderlance.Dto.Response;
using orderlance.Settings;

namespace orderlance.Services
{
    public class AuthService : IAuthService
    {
        private readonly UsersRepository _usersRepository;
        private readonly AppSettings _appSettings;
        
        public AuthService(UsersRepository usersRepository, IOptions<AppSettings> appSettings)
        {
            this._appSettings = appSettings.Value;
            this._usersRepository = usersRepository;
        }
        
        public AuthenticatedResponseDto Authenticate(string username, string password)
        {
            var result = new AuthenticatedResponseDto();
            var user = this._usersRepository.GetUserByEmail(username);

            if (user == null)
                return null;

            var userDataPayload = new UserDto()
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.Username,
                Role = user.Role,
                Avatar = user.Avatar,
            };
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, userDataPayload.Role),
                    new Claim("UserData", userDataPayload.ToJson()), 
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            
            var token = tokenHandler.CreateToken(tokenDescriptor);
            result.Token = tokenHandler.WriteToken(token);
            
            return result;
        }
    }
}