using System.Collections.Generic;
using System.Linq;
using orderlance.Abstract.Interfaces;
using orderlance.Dal.Entities;
using orderlance.Dal.Repositories;
using orderlance.Dto.Request.Meal;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Services
{
    public class MealsService : IMealsService
    {
        private readonly MealsRepository _mealsRepository;
        
        public MealsService(MealsRepository mealsRepository)
        {
            this._mealsRepository = mealsRepository;
        }
        
        public CollectionDto<MealResponseDto> GetMeals(PaginationParamsDto paramsDto, string categoryId = null)
        {
            var response = new CollectionDto<MealResponseDto>();
            IEnumerable<Meal> meals;
            
            if (paramsDto.Page == null || paramsDto.ItemsPerPage == null)
            {
                meals = this._mealsRepository.GetAllItems(categoryId);
                response.TotalItems = meals.Count();
            }
            else
            {
                meals = this._mealsRepository.GetItems(paramsDto.Page.Value, paramsDto.ItemsPerPage.Value, categoryId);
                response.TotalItems = this._mealsRepository.GetItemsCount(categoryId);
            }

            response.Page = paramsDto.Page ?? 0;
            response.Members = meals.Select(this.MapMeal);
            
            return response;
        }

        public MealResponseDto AddMeal(AddMealRequestDto requestDto)
        {
            var newMeal = new Meal
            {
                Name = requestDto.Name,
                Description = requestDto.Description,
                CategoryId = requestDto.CategoryId,
                Price = requestDto.Price,
                ImgUrl = requestDto.ImgUrl,
            };
            
            this._mealsRepository.StoreItem(newMeal);

            return this.MapMeal(newMeal);
        }

        public void EditMeal(EditMealRequestDto requestDto)
        {
            this._mealsRepository.UpdateMeal(new Meal
            {
                Id = requestDto.Id,
                Name = requestDto.Name, 
                Description = requestDto.Description, 
                Price = requestDto.Price, 
                ImgUrl = requestDto.ImgUrl, 
                CategoryId = requestDto.CategoryId, 
            });
        }

        public void RemoveMeal(string id)
        {
            this._mealsRepository.RemoveItem(id);
        }
        
        private MealResponseDto MapMeal(Meal meal)
        {
            return new MealResponseDto
            {
                Id = meal.Id,
                Name = meal.Name,
                Description = meal.Description,
                ImgUrl = meal.ImgUrl,
                Price = meal.Price,
                CategoryId = meal.CategoryId,
            };
        }
    }
}