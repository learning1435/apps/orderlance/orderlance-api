using System.Collections;
using System.Collections.Generic;
using System.Linq;
using orderlance.Abstract.Interfaces;
using orderlance.Dal.Entities;
using orderlance.Dal.Repositories;
using orderlance.Dto.Request.Category;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Services
{
    public class CategoriesService : ICategoriesService
    {
        private readonly CategoriesRepository _categoriesRepository;
        
        public CategoriesService(CategoriesRepository categoriesRepository)
        {
            this._categoriesRepository = categoriesRepository;
        }
        
        public CollectionDto<CategoryResponseDto> GetCategories(PaginationParamsDto paramsDto)
        {
            var response = new CollectionDto<CategoryResponseDto>();
            IEnumerable<Category> categories;
            
            if (paramsDto.Page == null || paramsDto.ItemsPerPage == null)
            {
                categories = this._categoriesRepository.GetAllItems();
                response.TotalItems = categories.Count();
            }
            else
            {
                categories = this._categoriesRepository.GetItems(paramsDto.Page.Value, paramsDto.ItemsPerPage.Value);
                response.TotalItems = this._categoriesRepository.GetItemsCount();
            }

            response.Page = paramsDto.Page ?? 0;
            response.Members = categories.Select(this.MapCategory);
            
            return response;
        }

        public CategoryResponseDto AddCategory(AddCategoryRequestDto requestDto)
        {
            var newCategory = new Category
            {
                Title = requestDto.Title,
                Description = requestDto.Description,
                ImgUrl = requestDto.ImgUrl,
            };
            
            this._categoriesRepository.StoreItem(newCategory);

            return this.MapCategory(newCategory);
        }

        public void EditCategory(EditCategoryRequestDto requestDto)
        {
            this._categoriesRepository.UpdateCategory(new Category
            {
                Id = requestDto.Id,
                Title = requestDto.Title,
                Description = requestDto.Description,
                ImgUrl = requestDto.ImgUrl,
            });
        }

        public void RemoveCategory(string id)
        {
            this._categoriesRepository.RemoveItem(id);
        }

        private CategoryResponseDto MapCategory(Category category)
        {
            return new CategoryResponseDto
            {
                Id = category.Id,
                Title = category.Title,
                Description = category.Description,
                ImgUrl = category.ImgUrl,
            };
        }
    }
}