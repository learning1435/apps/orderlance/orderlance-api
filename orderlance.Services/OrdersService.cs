using System;
using System.Collections.Generic;
using System.Linq;
using orderlance.Abstract.Interfaces;
using orderlance.Dal.Entities;
using orderlance.Dal.Repositories;
using orderlance.Dto.Request.Order;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;
using orderlance.Settings;

namespace orderlance.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly OrdersRepository _ordersRepository;
        private readonly IAuth _auth;

        public OrdersService(OrdersRepository ordersRepository, IAuth auth)
        {
            this._ordersRepository = ordersRepository;
            this._auth = auth;
        }

        public CollectionDto<OrderResponseDto> GetOrdersForUser(PaginationParamsDto paramsDto)
        {
            var userId = this._auth.User().Id;
            var response = new CollectionDto<OrderResponseDto>();
            IEnumerable<Order> orders;

            if (paramsDto.Page == null || paramsDto.ItemsPerPage == null)
            {
                orders = this._ordersRepository.GetAllItems(userId);
                response.TotalItems = orders.Count();
            }
            else
            {
                orders = this._ordersRepository.GetItems(paramsDto.Page.Value, paramsDto.ItemsPerPage.Value, userId);
                response.TotalItems = this._ordersRepository.GetItemsCount(userId);
            }

            response.Page = paramsDto.Page ?? 0;
            response.Members = orders.Select(this.MapOrder);

            return response;
        }
        
        public CollectionDto<OrderResponseDto> GetOrders(PaginationParamsDto paramsDto)
        {
            var response = new CollectionDto<OrderResponseDto>();
            IEnumerable<Order> orders;

            if (paramsDto.Page == null || paramsDto.ItemsPerPage == null)
            {
                orders = this._ordersRepository.GetAllItems();
                response.TotalItems = orders.Count();
            }
            else
            {
                orders = this._ordersRepository.GetItems(paramsDto.Page.Value, paramsDto.ItemsPerPage.Value);
                response.TotalItems = this._ordersRepository.GetItemsCount();
            }

            response.Page = paramsDto.Page ?? 0;
            response.Members = orders.Select(this.MapOrder);

            return response;
        }

        public OrderResponseDto StoreOrder(MakeOrderRequestDto requestDto)
        {
            var newOrder = new Order();
            newOrder.Paid = false;
            newOrder.PaymentMethod = requestDto.PaymentMethod;
            newOrder.Status = requestDto.PaymentMethod == PaymentMethod.CARD
                ? OrderStatus.PAID
                : OrderStatus.IN_DELIVERY;
            newOrder.OrderedAt = DateTime.Now;
            newOrder.UserId = this._auth.User().Id;
            newOrder.Items = requestDto.Meals.Select(orderItem => new OrderItem
            {
                Count = orderItem.Count,
                Name = orderItem.Meal.Name,
                Price = orderItem.Meal.Price,
            }).ToList();
            newOrder.Total = newOrder.Items.Select(item => item.Count * item.Price).Sum();
            
            this._ordersRepository.StoreItem(newOrder);

            return this.MapOrder(newOrder);
        }

        private OrderResponseDto MapOrder(Order order)
        {
            return new OrderResponseDto
            {
                Id = order.Id,
                Paid = order.Paid,
                Status = order.Status,
                OrderedAt = order.OrderedAt,
                PaidAt = order.PaidAt,
                UserId = order.UserId,
                PaymentMethod = order.PaymentMethod,
                Total = order.Total,
                Items = order.Items.Select(item => new OrderItemDto
                {
                    Price = item.Price,
                    Count = item.Count,
                    Name = item.Name,
                }).ToList(),
            };
        }
    }
}