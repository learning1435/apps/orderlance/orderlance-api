using MongoDB.Bson.Serialization.Attributes;

namespace orderlance.Dal.Entities
{
    public class Category : BaseEntity
    {
        [BsonElement("title")]
        public string Title { get; set; }
        
        [BsonElement("description")]
        public string Description { get; set; }
        
        [BsonElement("img_url")]
        public string ImgUrl { get; set; }
    }
}