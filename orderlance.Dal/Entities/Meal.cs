using MongoDB.Bson.Serialization.Attributes;

namespace orderlance.Dal.Entities
{
    public class Meal : BaseEntity
    {
        [BsonElement("name")]
        public string Name { get; set; }
        
        [BsonElement("description")]
        public string Description { get; set; }
        
        [BsonElement("img_url")]
        public string ImgUrl { get; set; }
        
        [BsonElement("price")]
        public double Price { get; set; }
        
        [BsonElement("category_id")]
        public string CategoryId { get; set; }
    }
}