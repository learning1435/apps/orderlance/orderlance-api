using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace orderlance.Dal.Entities
{
    public class User : BaseEntity
    {
        [BsonElement("username")]
        public string Username { get; set; }
        
        [BsonElement("email")]
        public string Email { get; set; }
        
        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("role")]
        public string Role { get; set; }
        
        [BsonElement("avatar")]
        public string Avatar { get; set; }
    }
}