using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace orderlance.Dal.Entities
{
    public class Order : BaseEntity
    {
        [BsonElement("user_id")]
        public string UserId { get; set; }
        
        [BsonElement("items")]
        public List<OrderItem> Items { get; set; }

        [BsonElement("ordered_at")]
        public DateTime OrderedAt { get; set; }
        
        [BsonElement("status")]
        public string Status { get; set; }
        
        [BsonElement("payment_method")]
        public string PaymentMethod { get; set; }
        
        [BsonElement("paid")]
        public bool Paid { get; set; }
        
        [BsonElement("paid_at")]
        public DateTime PaidAt { get; set; }

        [BsonElement("total")]
        public double Total { get; set; }
    }

    public class OrderItem
    {
        [BsonElement("name")]
        public string Name { get; set; }
        
        [BsonElement("count")]
        public int Count { get; set; }
        
        [BsonElement("price")]
        public double Price { get; set; }
    }
}