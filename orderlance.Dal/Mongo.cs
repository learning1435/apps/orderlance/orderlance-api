﻿using MongoDB.Driver;
using Microsoft.Extensions.Options;
using orderlance.Settings;

namespace orderlance.Dal
{
    public class Mongo
    {
        public readonly IMongoDatabase Database;

        public Mongo(IOptions<DatabaseSettings> databaseConfig)
        {
            var settings = databaseConfig.Value;
            var client = new MongoClient(settings.ConnectionString);
            
            this.Database = client.GetDatabase(settings.Database);
        }
    }
}
