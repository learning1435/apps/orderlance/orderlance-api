using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using orderlance.Dal.Entities;

namespace orderlance.Dal.Repositories
{
    public class OrdersRepository : BaseRepository<Order>
    {
        public OrdersRepository(Mongo database): base(database, "orders")
        {
        }
        
        public IEnumerable<Order> GetAllItems(string userId = null)
        {
            var items = this.Database.GetCollection<Order>(CollectionName);

            return userId == null ?
                items.Find(x => x.Id != null).Sort(Builders<Order>.Sort.Descending("ordered_at")).ToList() :
                items.Find(x => x.UserId == userId).Sort(Builders<Order>.Sort.Descending("ordered_at")).ToList();
        }
        
        public IEnumerable<Order> GetItems(int page = 1, int itemsPerPage = 50, string userId = null)
        {
            var items = this.Database.GetCollection<Order>(CollectionName);
            var queryBase = userId == null ?
                items.Find(x => x.Id != null).Sort(Builders<Order>.Sort.Descending("ordered_at")) :
                items.Find(x => x.UserId == userId).Sort(Builders<Order>.Sort.Descending("ordered_at"));
            
            return queryBase
                .Skip(itemsPerPage * (page - 1))
                .Limit(itemsPerPage)
                .ToList();
        }
        
        public int GetItemsCount(string userId = null)
        {
            var items = this.Database.GetCollection<Order>(CollectionName);

            return userId == null ? items.Find(x => x.Id != null).ToList().Count() : items.Find(x => x.UserId == userId).ToList().Count();
        }

        public void UpdateOrder(Order order)
        {
            var categories = this.Database.GetCollection<Order>(CollectionName);
            var filter = Builders<Order>.Filter.Eq("_id", BsonObjectId.Create(order.Id));
            var update = Builders<Order>.Update
                .Set("paid_at", order.PaidAt)
                .Set("paid", order.Paid);
            
            categories.UpdateOne(filter, update);
        }
    }
}