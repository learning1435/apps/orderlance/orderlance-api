using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using orderlance.Dal.Entities;

namespace orderlance.Dal.Repositories
{
    public class CategoriesRepository : BaseRepository<Category>
    {
        public CategoriesRepository(Mongo database): base(database, "categories")
        {
        }
        
        public void UpdateCategory(Category category)
        {
            var categories = this.Database.GetCollection<Category>(CollectionName);
            var filter = Builders<Category>.Filter.Eq("_id", BsonObjectId.Create(category.Id));
            var update = Builders<Category>.Update
                .Set("title", category.Title)
                .Set("description", category.Description)
                .Set("img_url", category.ImgUrl);
            
            categories.UpdateOne(filter, update);
        }
    }
}