using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using orderlance.Dal.Entities;

namespace orderlance.Dal.Repositories
{
    public class BaseRepository<T> where T: BaseEntity
    {
        protected readonly IMongoDatabase Database;
        protected readonly string CollectionName;
        
        protected BaseRepository(Mongo database, string collectionName)
        {
            this.Database = database.Database;
            this.CollectionName = collectionName;
        }

        public IEnumerable<T> GetAllItems()
        {
            var items = this.Database.GetCollection<T>(CollectionName);
            return items.Find(x => x.Id != null).ToList();
        }
        
        public IEnumerable<T> GetItems(int page = 1, int itemsPerPage = 50)
        {
            var items = this.Database.GetCollection<T>(CollectionName);
            return items.Find(x => x.Id != null)
                .Skip(itemsPerPage * (page - 1))
                .Limit(itemsPerPage)
                .ToList();
        }

        public int GetItemsCount()
        {
            var items = this.Database.GetCollection<T>(CollectionName);
            return items.Find(x => x.Id != null).ToList().Count;
        }
        
        public void StoreItem(T item)
        {
            var items = this.Database.GetCollection<T>(CollectionName);
            items.InsertOne(item);
        }

        public void RemoveItem(string id)
        {
            var items = this.Database.GetCollection<T>(CollectionName);
            var filter = Builders<T>.Filter.Eq("_id", BsonObjectId.Create(id));
            items.DeleteOne(filter);
        }
    }
}