using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using orderlance.Dal.Entities;

namespace orderlance.Dal.Repositories
{
    public class UsersRepository : BaseRepository<User>
    {
        public UsersRepository(Mongo database): base(database, "users")
        {
        }
        
        public User GetUserById(string id)
        {
            var users = this.Database.GetCollection<User>(CollectionName);
            return users.Find(x => x.Id == id).FirstOrDefault();
        }

        public User GetUserByEmail(string email)
        {
            var users = this.Database.GetCollection<User>(CollectionName);
            return users.Find(x => x.Email == email).FirstOrDefault();
        }

        public IEnumerable<User> GetAllUsers()
        {
            var users = this.Database.GetCollection<User>(CollectionName);
            return users.Find(x => x.Id != null).ToList();
        }

        public IEnumerable<User> GetAdmins()
        {
            var users = this.Database.GetCollection<User>(CollectionName);
            return users.Find(x => x.Role == "admin").ToList();
        }
        
        public void UpdateUser(User user)
        {
            var users = this.Database.GetCollection<User>(CollectionName);
            var filter = Builders<User>.Filter.Eq("_id", BsonObjectId.Create(user.Id));
            var update = Builders<User>.Update
                .Set("username", user.Username)
                .Set("avatar", user.Avatar);
            
            if (user.Role != null)
            {
                update = update.Set("role", user.Role);
            }
            
            users.UpdateOne(filter, update);
        }

        public void ChangeIsAdminRole(string userId, string role)
        {
            var user = this.GetUserById(userId);

            if (user == null) return;

            user.Role = role;
            
            this.UpdateUser(user);
        }
    }
}