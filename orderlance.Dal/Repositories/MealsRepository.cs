using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using orderlance.Dal.Entities;

namespace orderlance.Dal.Repositories
{
    public class MealsRepository : BaseRepository<Meal>
    {
        public MealsRepository(Mongo database): base(database, "meals")
        {
        }
        
        public IEnumerable<Meal> GetAllItems(string categoryId = null)
        {
            var items = this.Database.GetCollection<Meal>(CollectionName);

            return categoryId == null ? items.Find(x => x.Id != null).ToList() : items.Find(x => x.CategoryId == categoryId).ToList();
        }
        
        public IEnumerable<Meal> GetItems(int page = 1, int itemsPerPage = 50, string categoryId = null)
        {
            var items = this.Database.GetCollection<Meal>(CollectionName);
            var queryBase = categoryId == null ? items.Find(x => x.Id != null) : items.Find(x => x.CategoryId == categoryId);
            
            return queryBase
                .Skip(itemsPerPage * (page - 1))
                .Limit(itemsPerPage)
                .ToList();
        }
        
        public int GetItemsCount(string categoryId = null)
        {
            var items = this.Database.GetCollection<Meal>(CollectionName);

            return categoryId == null ? items.Find(x => x.Id != null).ToList().Count() : items.Find(x => x.CategoryId == categoryId).ToList().Count();
        }

        public void UpdateMeal(Meal meal)
        {
            var categories = this.Database.GetCollection<Meal>(CollectionName);
            var filter = Builders<Meal>.Filter.Eq("_id", BsonObjectId.Create(meal.Id));
            var update = Builders<Meal>.Update
                .Set("name", meal.Name)
                .Set("description", meal.Description)
                .Set("price", meal.Price)
                .Set("category_id", meal.CategoryId)
                .Set("img_url", meal.ImgUrl);
            
            categories.UpdateOne(filter, update);
        }
    }
}