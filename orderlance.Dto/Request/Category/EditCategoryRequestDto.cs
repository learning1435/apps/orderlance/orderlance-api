namespace orderlance.Dto.Request.Category
{
    public class EditCategoryRequestDto : BaseCategoryRequestDto
    {
        public string Id { get; set; }
    }
}