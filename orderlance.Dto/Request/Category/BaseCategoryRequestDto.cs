namespace orderlance.Dto.Request.Category
{
    public class BaseCategoryRequestDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
    }
}