namespace orderlance.Dto.Request.User
{
    public class ChangeProfileRequestDto
    {
        public string Username { get; set; }
        public string Avatar { get; set; }
    }
}