namespace orderlance.Dto.Request.User
{
    public class ChangePasswordRequestDto
    {
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}