namespace orderlance.Dto.Request
{
    public class CredentialsRequestDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}