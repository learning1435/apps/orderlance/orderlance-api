using System.Collections.Generic;
using orderlance.Dto.Response;

namespace orderlance.Dto.Request.Order
{
    public class MakeOrderRequestDto
    {
        public List<MakeOrderItemDto> Meals { get; set; }
        public string PaymentMethod { get; set; }
    }

    public class MakeOrderItemDto
    {
        public int Count { get; set; }
        public MealResponseDto Meal { get; set; }
    }
}