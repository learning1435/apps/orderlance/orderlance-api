namespace orderlance.Dto.Request.Meal
{
    public class BaseMealRequestDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public double Price { get; set; }
        public string CategoryId { get; set; }
    }
}