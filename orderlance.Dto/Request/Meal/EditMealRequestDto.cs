namespace orderlance.Dto.Request.Meal
{
    public class EditMealRequestDto : BaseMealRequestDto
    {
        public string Id { get; set; }
    }
}