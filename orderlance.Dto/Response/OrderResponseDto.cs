using System;
using System.Collections.Generic;

namespace orderlance.Dto.Response
{
    public class OrderResponseDto
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public List<OrderItemDto> Items { get; set; }
        public DateTime OrderedAt { get; set; }
        public string Status { get; set; }
        public string PaymentMethod { get; set; }
        public bool Paid { get; set; }
        public DateTime? PaidAt { get; set; }
        public double Total { get; set; }
    }

    public class OrderItemDto
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public double Price { get; set; }
    }
}