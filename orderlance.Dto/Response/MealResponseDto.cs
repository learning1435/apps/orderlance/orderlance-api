namespace orderlance.Dto.Response
{
    public class MealResponseDto
    {
        public string Id { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public string CategoryId { get; set; }
    }
}