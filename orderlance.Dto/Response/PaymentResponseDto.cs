using System;

namespace orderlance.Dto.Response
{
    public class PaymentResponseDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public float Amount { get; set; }
        public DateTime PaidAt { get; set; }
    }
}