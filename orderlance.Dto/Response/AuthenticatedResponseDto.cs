namespace orderlance.Dto.Response
{
    public class AuthenticatedResponseDto
    {
        public string Token { get; set; }
    }
}