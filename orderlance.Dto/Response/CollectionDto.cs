using System.Collections.Generic;

namespace orderlance.Dto.Response
{
    public class CollectionDto<T>
    {
        public int Page { get; set; } = 1;
        public int TotalItems { get; set; } = 0;
        public int ItemsPerPage { get; set; } = 50;
        public IEnumerable<T> Members { get; set; }
    }
}