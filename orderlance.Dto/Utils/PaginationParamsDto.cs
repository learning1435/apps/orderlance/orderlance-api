namespace orderlance.Dto.Utils
{
    public class PaginationParamsDto
    {
        public int? ItemsPerPage { get; set; } = 50;
        public int? Page { get; set; } = null;
    }
}