using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using orderlance.Abstract.Interfaces;
using orderlance.Settings;

namespace orderlance_api.Middlewares
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly string _secret;
        
        private readonly List<string> _exceptions = new List<string>
        {
            "/api/auth/login",
            "/api/auth/register",
        };

        public AuthMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            this._secret = appSettings.Value.Secret;
            this._next = next;
        }

        public async Task Invoke(HttpContext httpContext, IAuth auth, IUsersService usersService)
        {
            try
            {
                if (this._exceptions.Contains(httpContext.Request.Path))
                {
                    await _next.Invoke(httpContext);
                }
                else
                {
                    if (!httpContext.Request.Headers.ContainsKey("Authorization"))
                    {
                        throw new AuthenticationException();
                    }
                
                    var tokenHeaderValue = httpContext.Request.Headers["Authorization"].ToString();
                    tokenHeaderValue = tokenHeaderValue.Replace("Bearer ", "");
                    var tokenHandler = new JwtSecurityTokenHandler();

                    TokenValidationParameters validationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(this._secret)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };

                    tokenHandler.ValidateToken(tokenHeaderValue, validationParameters, out var securityToken);
                    var userIdClaims = (securityToken as JwtSecurityToken)?.Claims.FirstOrDefault(x => x.Type == "unique_name");
            
                    if (userIdClaims == null)
                        throw new AuthenticationException();

                    var userId = userIdClaims.Value;
            
                    var user = usersService.GetUserById(userId);
            
                    if (user == null)
                        throw new AuthenticationException();
            
                    auth.SetUser(user);
            
                    await _next.Invoke(httpContext);
                }
            }
            catch (Exception e)
            {
                var result = JsonConvert.SerializeObject(new { error = e.Message });
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = 401;
                await httpContext.Response.WriteAsync(result);

                return;
            }
        }
    }
}