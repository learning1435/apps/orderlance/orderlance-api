using Microsoft.AspNetCore.Builder;
using orderlance_api.Middlewares;

namespace orderlance_api.Extensions
{
    public static class AuthMiddlewareExtensions
    {

        public static IApplicationBuilder UseAuthMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthMiddleware>();
        }
    }
}