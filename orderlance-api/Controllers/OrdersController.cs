using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto.Request.Order;
using orderlance.Dto.Utils;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("/api/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            this._ordersService = ordersService;
        }

        [HttpGet("")]
        public IActionResult GetOrdersForUser([FromQuery] PaginationParamsDto paramsDto)
        {
            var response = this._ordersService.GetOrdersForUser(paramsDto);

            return Ok(response);
        }

        [HttpGet("all")]
        public IActionResult GetOrders([FromQuery] PaginationParamsDto paramsDto)
        {
            var response = this._ordersService.GetOrders(paramsDto);

            return Ok(response);
        }
        
        [HttpPost("")]
        public IActionResult StoreOrder(MakeOrderRequestDto requestDto)
        {
            var response = this._ordersService.StoreOrder(requestDto);

            return Ok(response);
        }
    }
}