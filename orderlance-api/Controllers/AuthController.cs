using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto.Request;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService userService)
        {
            _authService = userService;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Authenticate([FromBody]CredentialsRequestDto userParam)
        {
            var authenticated = _authService.Authenticate(userParam.Email, userParam.Password);

            if (authenticated == null)
                return BadRequest(new { message = "Email or password is incorrect" });

            return Ok(authenticated);
        }
    }
}