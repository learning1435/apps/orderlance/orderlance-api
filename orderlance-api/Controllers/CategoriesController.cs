using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto.Request.Category;
using orderlance.Dto.Utils;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("api/categories")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesService _categoriesService;
        
        public CategoriesController(ICategoriesService categoriesService)
        {
            this._categoriesService = categoriesService;
        }
        
        [HttpGet("")]
        public IActionResult GetCategories([FromQuery] PaginationParamsDto paramsDto)
        {
            var response = this._categoriesService.GetCategories(paramsDto);

            return Ok(response);
        }

        [HttpPost("")]
        public IActionResult StoreCategory([FromBody] AddCategoryRequestDto requestDto)
        {
            var response = this._categoriesService.AddCategory(requestDto);

            return Ok(response);
        }

        [HttpPut("")]
        public IActionResult UpdateCategory([FromBody] EditCategoryRequestDto requestDto)
        {
            this._categoriesService.EditCategory(requestDto);

            return Ok();
        }

        [HttpDelete("{categoryId}")]
        public IActionResult RemoveCategory(string categoryId)
        {
            this._categoriesService.RemoveCategory(categoryId);

            return Ok();
        }
    }
}