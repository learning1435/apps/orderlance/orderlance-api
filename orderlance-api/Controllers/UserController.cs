using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto.Request.User;
using orderlance.Dto.Utils;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("/api/user")]
    public class UserController : ControllerBase
    {
        private readonly IUsersService _usersService;
        
        public UserController(IUsersService usersService)
        {
            this._usersService = usersService;
        }
        
        [HttpGet("")]
        public IActionResult GetUsers([FromQuery] PaginationParamsDto paramsDto)
        {
            var response = this._usersService.GetUsers(paramsDto);

            return Ok(response);
        }
        
        [HttpPut("")]
        public IActionResult ChangeProfile([FromBody] ChangeProfileRequestDto requestDto)
        {
            this._usersService.UpdateProfile(requestDto);

            return Ok();
        }
        
        [HttpPut("password")]
        public IActionResult ChangePassword([FromBody] ChangePasswordRequestDto requestDto)
        {
            this._usersService.ChangePassword(requestDto);

            return Ok();
        }
    }
}