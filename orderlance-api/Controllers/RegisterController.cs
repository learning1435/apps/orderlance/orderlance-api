using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto;
using orderlance.Dto.Request;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class RegisterController : ControllerBase
    {
        private readonly IUsersService _usersService;
        
        public RegisterController(IUsersService usersService)
        {
            this._usersService = usersService;
        }
        
        [Route("register")]
        [HttpPost]
        public ActionResult Register([FromBody] RegisterRequestDto registerRequestDto)
        {
            this._usersService.RegisterUser(registerRequestDto);

            return Ok();
        }
    }
}