using Microsoft.AspNetCore.Mvc;
using orderlance.Abstract.Interfaces;
using orderlance.Dto.Request.Meal;
using orderlance.Dto.Utils;

namespace orderlance_api.Controllers
{
    [ApiController]
    [Route("api/meals")]
    public class MealsController : ControllerBase
    {
        private readonly IMealsService _mealsService;
        
        public MealsController(IMealsService mealsService)
        {
            this._mealsService = mealsService;
        }
        
        [HttpGet("")]
        public IActionResult GetCategories([FromQuery] PaginationParamsDto paramsDto, string categoryId = null)
        {
            var response = this._mealsService.GetMeals(paramsDto, categoryId);

            return Ok(response);
        }

        [HttpPost("")]
        public IActionResult StoreMeal([FromBody] AddMealRequestDto requestDto)
        {
            var response = this._mealsService.AddMeal(requestDto);

            return Ok(response);
        }
        
        [HttpPut("")]
        public IActionResult UpdateMeal([FromBody] EditMealRequestDto requestDto)
        {
            this._mealsService.EditMeal(requestDto);

            return Ok();
        }

        [HttpDelete("{mealId}")]
        public IActionResult RemoveMeal(string mealId)
        {
            this._mealsService.RemoveMeal(mealId);

            return Ok();
        }
    }
}