using orderlance.Dto;
using orderlance.Dto.Request;
using orderlance.Dto.Request.User;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Abstract.Interfaces
{
    public interface IUsersService
    {
        void RegisterUser(RegisterRequestDto requestDto);

        CollectionDto<UserDto> GetUsers(PaginationParamsDto paramsDto);

        UserDto GetUserById(string id);

        void UpdateProfile(ChangeProfileRequestDto requestDto);
        
        void ChangePassword(ChangePasswordRequestDto requestDto);
    }
}