using System.Collections.Generic;
using orderlance.Dto.Request.Meal;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Abstract.Interfaces
{
    public interface IMealsService
    {
        CollectionDto<MealResponseDto> GetMeals(PaginationParamsDto paramsDto, string categoryId = null);
        
        MealResponseDto AddMeal(AddMealRequestDto requestDto);
        
        void EditMeal(EditMealRequestDto requestDto);

        void RemoveMeal(string id);
    }
}