using orderlance.Dto;
using orderlance.Dto.Response;

namespace orderlance.Abstract.Interfaces
{
    public interface IAuth
    {
        void SetUser(UserDto user);
        
        UserDto User();
    }
}