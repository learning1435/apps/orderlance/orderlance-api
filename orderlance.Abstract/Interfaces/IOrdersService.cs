using orderlance.Dto.Request.Order;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Abstract.Interfaces
{
    public interface IOrdersService
    {
        CollectionDto<OrderResponseDto> GetOrdersForUser(PaginationParamsDto paramsDto);
        
        CollectionDto<OrderResponseDto> GetOrders(PaginationParamsDto paramsDto);

        OrderResponseDto StoreOrder(MakeOrderRequestDto requestDto);
    }
}