using orderlance.Dto;
using orderlance.Dto.Response;

namespace orderlance.Abstract.Interfaces
{
    public interface IAuthService
    {
        AuthenticatedResponseDto Authenticate(string username, string password);
    }
}