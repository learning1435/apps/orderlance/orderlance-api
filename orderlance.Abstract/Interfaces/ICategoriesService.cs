using System.Collections.Generic;
using orderlance.Dto.Request.Category;
using orderlance.Dto.Response;
using orderlance.Dto.Utils;

namespace orderlance.Abstract.Interfaces
{
    public interface ICategoriesService
    {
        CollectionDto<CategoryResponseDto> GetCategories(PaginationParamsDto paramsDto);
        
        CategoryResponseDto AddCategory(AddCategoryRequestDto requestDto);
        
        void EditCategory(EditCategoryRequestDto requestDto);

        void RemoveCategory(string id);
    }
}